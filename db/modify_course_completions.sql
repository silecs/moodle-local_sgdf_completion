ALTER TABLE `course_completions`
ADD `sgdfstatus` bigint(10) NOT NULL DEFAULT 0 COMMENT 'si négatif, en erreur',
ADD `sgdftime` bigint(10) NOT NULL DEFAULT 0 COMMENT 'date de la dernière remontée' AFTER `sgdfstatus`,
ADD `sgdfformationid` bigint(10) NOT NULL DEFAULT 0  COMMENT 'formationid pour l api SGDF' AFTER `sgdftime`,
ADD `sgdferror` text NOT NULL COMMENT 'erreur retournée par l api';

