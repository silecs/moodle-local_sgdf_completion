<?php

$tasks = [
    [
        'classname' => 'local_sgdf_completion\task\export_completion_task',
        'blocking' => 0,
        'minute' => '30',
        'hour' => '3',
        'day' => '*',
        'month' => '*',
        'dayofweek' => '*',
    ],
];
