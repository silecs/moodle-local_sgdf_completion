<?php
/**
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'SGDF completion export';
