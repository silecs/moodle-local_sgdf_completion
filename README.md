# Plugin `moodle-local_sgdf_completion`

Remonte les informations de cours suivis par les utilisateurs dans l'API SGDF.


Nécessite les paramètres suivants dans `config.php` :

```
$CFG->sgdfparams = [
// appelant applicatif (pour sgdf_completion...)
'appelantAppli' => '********-****-****-****-************',  
'passappelantAppli' => '************',

'audience' => '********************************',

'oauthurl' => 'https://dev-intranetapi.sgdf.fr/oauth2/token',       # qualif-https OAUTH2
'apiurl' => 'https://dev-intranetapi.sgdf.fr/api/v1',  # qualif-https API
];
```

