#!/usr/bin/env php
<?php 

define('TOKENFILE', 'sgdftoken.json');
$config = require 'sgdfconf.php';
// print_r($configig);

$helpmsg = <<< HELP
    Usage : $argv[0] commande [paramètres optionnels]
    token       récupère un token d'authentification et l'enregistre
    loadtoken   relit le fichier token et affiche le contenu (test / débogage)

    info ADHNUMBER  VERBOSE(bool)       affiche les informations sur l'adhérent
    formations ADHNUMBER VERBOSE(bool)  affiche les formations suivies par l'adhérent
    postformation ADHNUMBER             envoie la formation suivie à l'API (définie dans le fichier de config)
    formationlieux VERBOSE(bool)        liste les lieux de formation (long)
    
    HELP;

if (! isset($argv[1])) {
    echo $helpmsg;
    return 1;
}

switch ($argv[1]) {
    case 'token':
        echo get_token() . "\n\n";
        // analyse du token avec https://jwt.io/
        break;

    case 'loadtoken':
        echo load_token() . "\n\n";
        break;

    case 'info':
        echo get_info($argv[2], $argv[3]);
        break;

    case 'formations':
        print_r(get_formations($argv[2], $argv[3]));
        break;

    case 'postformation':
        post_formation($argv[2]);
        break;

    case 'formationlieux':
        print_r(get_formationlieux($argv[2]));
        break;
    
    default:
        echo $helpmsg;
        break;
}
return 0;



    /**
     * récupère un token d'authentification et le sauve dans le fichier sgdftoken.json
     * @global type $config
     * @return boolean or string (token)
     */
    function get_token() {
        global $config;

        $ch = curl_init();
        $data = [
            'client_id' => $config['appelant'],
            'audience' => $config['audience'],
            'username' => $config['appelant'],
            'password' => $config['passappelant'],
            'grant_type' => 'password',
            ];
        curl_setopt_array($ch, [
            CURLOPT_URL => $config['urloauth'],
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => ['Accept: application/json'],
            CURLOPT_POSTFIELDS => http_build_query($data, '', '&')
            ]);
        $res = curl_exec($ch);
        $result = json_decode($res);

        if (isset($result->error)) {
            $this->error = $result->error;
            $this->error_description = $result->error_description;
            return false;
        }
        $access_token = $result->access_token;
        $refresh_token = $result->refresh_token;
        file_put_contents(TOKENFILE, $res, 0);

        return $access_token;
    }

    function load_token() {
        $res = json_decode(file_get_contents(TOKENFILE));
        return $res->access_token;
    }


    /**
     *
     * @global array $config
     * @param string $adhnumber
     * @param bool $verbose
     * @return array of string
     */
    function get_formations($adhnumber, $verbose) {
        global $config;
        $url = sprintf('%s/adherents/%d/formations', $config['urlapi'], $adhnumber);
        $res = smartcurl(load_token(), $url, null);
        if ($verbose) {
            print_r($res);
        }
        foreach ($res->data as $jformation) {
            $result[$jformation->id] = sprintf("%s (%s)", $jformation->libelle, $jformation->dateFin);
        }
        return $result;
    }

    /**
     * 
     * @global array $config
     * @param int or string $adhnumber
     * @param bool $verbose
     * @return string
     */
    function get_info($adhnumber, $verbose) {
        global $config;
        $url = sprintf('%s/adherents/%d', $config['urlapi'], $adhnumber);
        $res = smartcurl(load_token(), $url, null);
        if ($verbose) {
            print_r($res);
        }
        return sprintf("\n%s %s (%s)\n",
            $res->data->prenom,
            $res->data->nom,
            $res->data->nomNaissance);
    }


    /**
     * remonte une formation à l'API (définie dans le fichier de conf)
     * @global array $config
     * @param string $adhnumber
     */
    function post_formation($adhnumber) {
        global $config;
        $data = $config['postdata'];
        $data['codeAdherent'] = $adhnumber;
        $url = sprintf('%s/formations/', $config['urlapi']);
        $res = smartcurl(load_token(), $url, $data);
        var_dump($res);
    } 

    /**
     *
     * @global array $config
     * @param bool $verbose
     * @return array of string
     */
    function get_formationlieux($verbose) {
        global $config;
        $url = sprintf('%s/formations/lieux', $config['urlapi']);
        $res = smartcurl(load_token(), $url, null);
        if ($verbose) {
            print_r($res);
        }
        $result=[];
        foreach ($res->data as $lieu) {
            $result[$lieu->id] = sprintf("%s (%s %s)", $lieu->libelle, $lieu->adresse->codePostal, $lieu->adresse->ville);
        }
        return $result;
    }

    function smartcurl($access, $url, $postdata=null) {
        global $config;
        $ch = curl_init();
        $headers = [
            'Accept: application/json',
            'idAppelant: ' . $config['appelant'],
            'Authorization: bearer ' . $access
            ];
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_POST => ! ($postdata === null),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            ]);
        if (isset($postdata)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata, '', '&'));
        }
        $res = curl_exec($ch);
        $info = curl_getinfo($ch); // débogage
        //print_r($info);
        $result = json_decode($res);
        return $result;
    }


