<?php
/**
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
use \local_sgdf_completion\reporting;
use \local_sgdf_completion\process_completion;

require(__DIR__ . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');

require_login();

/* @var $PAGE moodle_page */
global $PAGE, $OUTPUT;

$PAGE->set_context(context_system::instance());
$PAGE->set_url("{$CFG->wwwroot}/local/sgdf_completion/viewreport.php");
$PAGE->set_pagelayout('report');

$titre = 'Cours suivis - statistiques';
$PAGE->set_title($titre); // tab title
$PAGE->set_heading($titre); // titre haut de page
$PAGE->navbar->add('Statistiques');

echo $OUTPUT->header();

$headers = ['ID Cours', 'Cours', 'Nombre', 'Premier', 'Dernier', 'remonteintranet'];

$table = new html_table();
$table->head = $headers;
$table->data = reporting::get_course_statistics();
echo html_writer::table($table);

$completionp = new process_completion(1, 0);
$metadata = $completionp->get_course_metadata();
print_r($metadata);

if ($completionp->error) {
    throw new moodle_exception($completionp->errormessage);
}

echo $OUTPUT->footer();
