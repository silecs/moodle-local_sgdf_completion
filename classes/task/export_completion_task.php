<?php
namespace local_sgdf_completion\task;

use \local_sgdf_completion\process_completion;

/**
 * cette classe trouve les adhérents SGDF ayant suivi (completion) un cours récemment
 * et exporte l'information dans leur compte vers l'API SGDF
 */
class export_completion_task extends \core\task\scheduled_task {

    /**
     * Return the task's name as shown in admin screens.
     *
     * @return string
     */
    public function get_name()
    {
        return 'Exporter les adhérents ayant terminé un cours';
    }

    /**
     * Execute the task.
     */
    public function execute()
    {
        $process = new process_completion(2, 0);
        $process->run_all_courses();
    }

}
