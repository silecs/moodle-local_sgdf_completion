<?php
/**
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use \local_sgdf_completion\process_completion;
use \local_sgdf_completion\process_adherent;

define('CLI_SCRIPT', true);
require(dirname(dirname(dirname(__DIR__))).'/config.php'); // global moodle config file.
require_once($CFG->libdir . '/clilib.php');      // cli only functions

list($options, $unrecognized) = cli_get_params(
        ['help' => false, 'verbose' => 1, 'limit' => 0, 'course' => 0,
         'run' => false, 'run-diagnostic' => false, 'reset-exports' => false, 
         'fonction' => false, 'adh' => 0,
        ],
    );

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

$help =
"Exporte les cours suivis dans l'API SGDF.
 Normalement, ce script ne sert qu'à l'initialisation, et ensuite le processus passe en cron (tâche planifiée).

Options:
--verbose=N           Verbosité (0 to 3), 1 par défaut
--limit=0             Limite (0=aucune)
--course=6            Pour restreindre à un cours (débogage)
--help                Affiche l'aide

--run                 Lance l'export
--run-diagnostic      Affiche un diagnostic détaillé avant export (aucune modification sur Moodle ni sur l'API)
--reset-exports       Efface les données d'export dans les tables (mise au point)

--fonction            Affiche la fonction de l'adhérent
--adh=123456789

";

if ( ! empty($options['help']) ) {
    echo $help;
    return 0;
}

if ( $options['run-diagnostic'] ) {
    $process = new process_completion($options['verbose'], $options['limit']);
    $process->init_course_export($options['course']);
    $process->run_course_diagnostic();
    return 0;
}

if ( $options['run'] ) {
    $process = new process_completion($options['verbose'], $options['limit']);
    if ($options['course'] > 0) {
        $process->init_course_export($options['course']);
        $process->run_course_export();
    } else {
        $process->run_all_courses();
    }
    return 0;
}

if ( $options['reset-exports'] ) {
    $process = new process_completion($options['verbose'], $options['limit']);
    $process->reset_exports();
    return 0;
}

if ( $options['fonction'] ) {
    $proc = new process_completion($options['verbose'], $options['limit']);
    $adherent = new process_adherent($options['verbose'], 0, $proc->token, $options['adh']);
    print_r($adherent->get_fonction());
    return 0;
}
