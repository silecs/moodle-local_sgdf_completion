<?php
/**
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2021092000;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2020061502;        // Requires this Moodle version
$plugin->component = 'local_sgdf_completion';       // Full name of the plugin (used for diagnostics)
