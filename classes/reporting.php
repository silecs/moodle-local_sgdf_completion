<?php
/**
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_sgdf_completion;

/**
 * Methods to display statistics about course completions
 */
class reporting
{
    private $verbose = 0;
    const SgdfIntranetCategory = 2;

    /**
     *
     */
    function __construct()
    {
        //$this->verbose = $verbose;
    }


    /**
     * liste des cours
     * @global \moodle_database $DB
     * @return array(array(string)) : table rows
     */
    public static function get_course_statistics()
    {
        global $DB;
        $res = [];
        $sql = "SELECT course, shortname, "
            . "DATE(FROM_UNIXTIME(MIN(timecompleted))) AS mindate, DATE(FROM_UNIXTIME(MAX(timecompleted))) AS maxdate, COUNT(userid) AS cnt "
            . "FROM course_completions CC "
            . "JOIN course ON (course.id = CC.course) "
            . "WHERE timecompleted > 0 "
            . "GROUP BY course";
        $records = $DB->get_records_sql($sql);

        foreach ($records as $record) {
            $urlcourse = new \moodle_url('/course/view.php', ['id' => $record->course]);
            $res[] = [
                    $record->course,
                    \html_writer::link($urlcourse, $record->shortname),
                    $record->cnt,
                    $record->mindate,
                    $record->maxdate,
                    self::get_remonteintranet($record->course),
                ];
        }
        return $res;
    }

    /**
     * @param int $courseid
     * @global \moodle_database $DB
     * @return int (0 | 1)
     */
    private static function get_remonteintranet(int $courseid)
    {
        global $DB;
        $sql = "SELECT intvalue "
            . "FROM customfield_field cf "
            . "LEFT JOIN customfield_data cd on (cd.fieldid = cf.id AND instanceid = ?) "
            . "WHERE shortname='remonteintranet' ";
        return $DB->get_field_sql($sql, [$courseid]);
    }


/*** OBSOLETE  methods ****/

    /**
     * liste des données intranet SGDF pour un cours
     * @param int $courseid
     * @global \moodle_database $DB
     * @return array
     */
    private static function get_intranet_data(int $courseid)
    {
        global $DB;
        $sql = "SELECT shortname, COALESCE(charvalue, value) AS cvalue "
            . "FROM customfield_field cf "
            . "LEFT JOIN customfield_data cd on (cd.fieldid = cf.id AND instanceid = ?) "
            . "WHERE categoryid = ?";
        $records = $DB->get_records_sql_menu($sql, [$courseid, self::SgdfIntranetCategory]);
        return $records;
    }

    /**
     * liste des champs intranet SGDF définis (de base remonteintranet, typeformation, libellereconnaissance)
     * @global \moodle_database $DB
     * @return array
     */
    public static function get_intranet_fields()
    {
        global $DB;
        $res = $DB->get_fieldset_select('customfield_field', 'shortname', 'categoryid = ?', [self::SgdfIntranetCategory]);
        return $res;
    }

}