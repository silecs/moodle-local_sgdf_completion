<?php
/**
 * Settings and links
 *
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$ADMIN->add('reports', 
            new admin_externalpage(
                'local_sgdf_completion_viewreport',
                "SGDF cours suivis",
                "$CFG->wwwroot/local/sgdf_completion/viewreport.php",
                'local/sgdf_completion:viewreport'
                )
        );

// no report settings
$settings = null;
