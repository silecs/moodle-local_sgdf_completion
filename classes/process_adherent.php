<?php
/**
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_sgdf_completion;

// Toutes les méthodes qui ne concernent qu'un seul adhérent
// Appelé depuis process_completion ou d'autres classes

class process_adherent
{
    public $params = []; // paramètres de configuration SGDF (fichier config.php)
    public $verbose = 0;
    public $limit = 0;

    public $token = ''; // authentication token oauth
    public $processtime = 0;
    public $courseid = 0;

    // retour de l'export par l'api pour un adhérent
    public $error = false;
    public $error_description = '';
    public $error_json = ''; // json retourné par l'API SGDF.
    public $formationid = 0;

    private $completionid;
    private $adhnumber; // adherent number (9 digits, eg. 163449640)
    private $postparams;

    /**
     * 
     * @global array $CFG
     * @param int $verbose
     */
    public function __construct(int $verbose, int $processtime, string $token, int $adhnumber)
    {
        global $CFG;

        if ( ! preg_match('/[0-9]{9}/', $adhnumber)) {
            die ("adherent id = $adhnumber non conforme");
        }
        $this->params = $CFG->sgdfparams;

        $this->verbose = $verbose;
        $this->processtime = $processtime;
        $this->token = $token;
        $this->adhnumber = $adhnumber;
        $this->reset_errors();
    }


    /**
     *
     * @param array $adhlog modifié
     */
    public function export_completion(array $postparams, int $completionid)
    {
        $this->postparams = $postparams;
        $this->completionid = $completionid;

        // interrompt si la formation est déjà remontée (anti-doublon)

        // export effectif
        $diag = $this->low_post_completion();
        $this->update_course_completions($diag);

    }

    private function low_post_completion()
    {
        $data = $this->postparams;
        $data['codeAdherent'] = $this->adhnumber;
        $url = sprintf('%s/formations/', $this->params['apiurl']);
        $result = $this->smartcurl($url, $data);

        if ($result) {
            $this->formationid = $result->data;
            return true;
        } else {
            $this->formationid = 0;
            return false;
        }
    }

    /**
     *
     * @global \moodle_database $DB
     * @param boolean $ok
     * @return boolean
     */
    private function update_course_completions($ok)
    {
        global $DB;
        $record = $DB->get_record('course_completions', ['id' => $this->completionid], '*', MUST_EXIST);

        $record->sgdftime = $this->processtime;
        if ($ok) {
            $record->sgdfstatus = 1;
            $record->sgdfformationid = $this->formationid;
        } else {
            $record->sgdfstatus = $record->sgdfstatus - 1; // nb de retrys, en négatif
            $record->sgdfformationid = 0;
            $record->sgdferror = $this->error_json;
        }
        $DB->update_record('course_completions', $record);
        return true;
    }

    private function reset_errors()
    {
        $this->error = false;
        $this->error_description = '';
        $this->error_json = '';
    }


    /**
     *
     * @return array of string ([formationid => libelle])
     */
    public function get_formations()
    {
        $url = sprintf('%s/adherents/%d/formations', $this->params['apiurl'], $this->adhnumber);
        $res = $this->smartcurl($url, null);
        if ($this->verbose > 2) {
            print_r($res);
        }
        foreach ($res->data as $jformation) {
            $result[$jformation->id] = $jformation->libelle;
        }
        return $result;
    }

    /**
     *
     * @return string
     */
    public function get_information()
    {
        $url = sprintf('%s/adherents/%d', $this->params['apiurl'], $this->adhnumber);
        $res = $this->smartcurl($url, null);
        if ($this->verbose > 2) {
            print_r($res);
        }
        return sprintf("%s  %s %s (%s)\n",
            $this->adhnumber,
            $res->data->prenom,
            $res->data->nom,
            $res->data->nomNaissance);
    }

    /**
     *
     * @return array  eg. [ '980' => 'SALARIE' ]
     */
    public function get_fonction()
    {
        $url = sprintf('%s/adherents/%d', $this->params['apiurl'], $this->adhnumber);
        $res = $this->smartcurl($url, null);
        if ($this->verbose > 2) {
            print_r($res);
        }
        $fonction = $res->data->fonctionPrincipale->fonction;
        return [$fonction->code => $fonction->nom];
    }
   
    private function smartcurl($url, $postdata=null)
    {
        $ch = curl_init();
        $headers = [
            'Accept: application/json',
            'idAppelant: ' . $this->params['appelantAppli'],
            'Authorization: bearer ' . $this->token
            ];
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_POST => ! ($postdata === null),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            ]);
        if (isset($postdata)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata, '', '&'));
        }
        $res = curl_exec($ch);
        $result = json_decode($res);
        if (isset($result->errors)) {
            $this->error = true;
            $this->error_json = json_encode($result->errors, JSON_UNESCAPED_UNICODE);
            return false;
            // print_r(curl_getinfo($ch));
        }
        return $result;
    }

    /**
     * helper function to display a character on a progressbar
     * @param int $minverb minimal verbosity to display character
     * @param string $text (can be a single char or even a short string)
     */
    private function vecho(int $minverb, string $text) {
        if ($this->verbose >= $minverb) echo $text; // progress bar
    }
}
