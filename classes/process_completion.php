<?php
/**
 * @package    local_sgdf_completion
 * @copyright  2020-2021 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_sgdf_completion;

class process_completion
{
    public $params = []; // paramètres de configuration SGDF (fichier config.php)
    public $verbose = 0;
    public $limit = 0;

    public $token = ''; // authentication token oauth
    public $processtime = 0;
    public $courseid = 0;

    // retour tous adhérents
    // inutilisé public $adhlog = [];


    /**
     * paramètres à passer à l'API en écriture. Constantes scalaires,
     * null pour les paramètres à adapter.
     * @var array
     */
    private $apiparams = [
        'role' => 0,
        'commentaire' => '',
        'idLieu' => 2304,
        'codeAdherent' => null, // = résolution de course_completions.userid
        'dateFin' => null, // datetime de lancement du script au format "2020-31-12T00:00:00"
        'libelle' => null, // récupéré dans les métadonnées du cours avec la clé « libellereconnaissance »
        'typeFormation' => null, //récupéré dans les métadonnées du cours avec la clé « typeformation »
    ];

    private $postparams;

    const METADATAFIELDS = [ 'remonteintranet', 'typeformation', 'libellereconnaissance' ];

    /**
     * 
     * @global array $CFG
     * @param int $verbose
     * @param int $limit
     */
    public function __construct(int $verbose, int $limit)
    {
        global $CFG;
        $this->params = $CFG->sgdfparams;
        $this->verbose = $verbose;
        $this->limit = $limit;
        $this->get_token();
    }

    public function run_all_courses()
    {
        foreach($this->get_target_courses() as $courseid) {
            echo "\ncours $courseid\n";
            $this->init_course_export($courseid);
            $this->run_course_export();
        }
    }

    public function init_course_export(int $courseid)
    {
        $this->postparams = $this->apiparams;
        $this->courseid = $courseid;
        $metadata = $this->get_course_metadata();
        $this->postparams['libelle'] = $metadata[$courseid]['libellereconnaissance'];
        $this->postparams['typeFormation'] = $metadata[$courseid]['typeformation'];
        $this->postparams['dateFin'] = substr(date('c'), 0, 19);
        $this->processtime = time();
        $this->adhlog = ['error' => [], 'ok' => []];
    }

    /**
     * affiche tous les adhérents à exporter dans le prochain run, pour débogage et tests
     */
    public function run_course_diagnostic()
    {
        $adherents = $this->get_adherents_to_export();
        print_r($adherents);
        foreach ($adherents as $ccid => $adhnumber) {
            $adherent = new process_adherent($this->verbose, $this->processtime, $this->token, $adhnumber);
            printf("%6d  %s\n", $ccid, $adherent->get_information());
            print_r($adherent->get_formations($adhnumber));
        }
    }

    /**
     * affiche tous les adhérents à exporter dans le prochain run, pour débogage et tests
     */
    public function run_course_export()
    {
        $adherents = $this->get_adherents_to_export();
        $this->vecho(1, count($adherents) . " adhérents à exporter.\n");
        foreach ($adherents as $ccid => $adhnumber) {
            $adherent = new process_adherent($this->verbose, $this->processtime, $this->token,  $adhnumber);
            $this->vecho(2, '.');
            $adherent->export_completion($this->postparams, $ccid);
        }

        echo "\n\n";
        echo $this->display_completion_diagnostic(true);
        echo "\n\n";
        echo $this->display_completion_diagnostic(false);
        $this->log_export();
    }

    /**
     *
     * @global \moodle_database $DB
     * @return array ['ccid' => 'adherent-number']
     */
    private function get_adherents_to_export()
    {
        global $DB;
        $sql = "SELECT CC.id as ccid, username  FROM {course_completions} CC "
            . "JOIN {user} U ON (U.id=CC.userid) "
            . "WHERE course=%d AND timecompleted > 0  AND sgdfstatus < 1  AND username RLIKE '[0-9]{9}'";
        $sql = sprintf($sql, $this->courseid);
        if ($this->limit > 0) {
            $sql .= sprintf(" LIMIT %d", $this->limit);
        }
        return $DB->get_records_sql_menu($sql);
    }


    private function display_completion_diagnostic($ok = true)
    {
        global $DB;
        $output = '';

        $flag = [true => 'OK', false => 'EN ERREUR'];
        $sql = "SELECT CC.id as ccid, CC.sgdferror, CC.sgdfstatus, U.id AS uid, username, firstname, lastname  FROM {course_completions} CC "
            . "JOIN {user} U ON (U.id=CC.userid) "
            . "WHERE course=? AND timecompleted > 0 AND sgdftime=?";
        $sql = $sql .  ($ok ? "AND sgdfstatus > 0 " : "AND sgdfstatus < 0 ");
        $users = $DB->get_records_sql($sql, [$this->courseid, $this->processtime]);

        echo count($users) . " adhérents exportés " . $flag[$ok] . " pour l'api : \n";
        foreach ($users as $user) {
            $output .= sprintf("%6d  %d  %s %s  ", $user->uid, $user->username, $user->firstname, $user->lastname);
            if ($user->sgdfstatus < 0) {
                $output .= sprintf("%d  %s", $user->sgdfstatus, $user->sgdferror);
            }
            $output .= "\n";
        }
        return $output;
    }


    /**
     * récupère un token d'authentification
     * @return false or string (token)
     */
    private function get_token() {
        $ch = curl_init();
        $data = [
            'client_id' => $this->params['appelantAppli'],
            'audience' => $this->params['audience'],
            'username' => $this->params['appelantAppli'],
            'password' => $this->params['passappelantAppli'],
            'grant_type' => 'password',
            ];
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->params['oauthurl'],
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => ['Accept: application/json'],
            CURLOPT_POSTFIELDS => http_build_query($data, '', '&')
            ]);
        $res = curl_exec($ch);
        $result = json_decode($res);
        // print_r($result);
        if (isset($result->error)) {
            $this->error = $result->error;
            $this->error_description = $result->error_description;
            echo "TOKEN ERROR :\n";
            printf("%s: %s\n\n", $this->error, $this->error_description);
            die();
            return false;
        }
        $this->token = $result->access_token;

        return $this->token;
    }

    /**
     * renvoie les cours concernés par la remontée
     * @return array
     * @global \moodle_database $DB
     */
    private function get_target_courses()
    {
        global $DB;
        $sql = "SELECT instanceid FROM customfield_data cd "
            . "JOIN customfield_field cf ON (cf.id = cd.fieldid) "
            . "WHERE cf.shortname='remonteintranet' AND cd.value=1";
        return $DB->get_fieldset_sql($sql);
    }

    /**
     * renvoie les métadonnées pour les cours remontés
     * @return array
     * @global \moodle_database $DB
     */
    public function get_course_metadata()
    {
        global $DB;
        $res = [];
        $sql = "SELECT shortname, COALESCE(charvalue, value) AS cvalue, instanceid AS courseid "
            . "FROM customfield_field cf JOIN customfield_data cd on (cd.fieldid = cf.id) "    // LEFT JOIN ?
            . "AND instanceid IN ( %s )";
        $rows = $DB->get_records_sql(sprintf($sql, join(',', $this->get_target_courses())));

        foreach ($rows as $row) {
            $res[$row->courseid][$row->shortname] = $row->cvalue;
        }
        $this->check_course_metadata($res);
        return $res;
    }

    /**
     * Vérifie les métadonnées et signale une erreur en cas de lacune (champ obligatoire non défini
     * @param array $results
     */
    private function check_course_metadata($results)
    {
        foreach ($results as $courseid => $data) {
           foreach (self::METADATAFIELDS as $field) {
                if (! isset($data[$field])) {
                    $this->error = true;
                    $this->errormessage .= "'$field' absent pour le cours $courseid. \n";
                    continue;
                }
                if ($data[$field] == null) {
                    $this->error = true;
                    $this->errormessage .= "'$field' vide pour le cours $courseid. \n";
                }
           }
        }
    }

    // efface les données d'export (colonnes sgdf* dans course_completions et enregistrements dans sgdf_log)
    public function reset_exports()
    {
        global $DB;

        $DB->execute("UPDATE {course_completions} SET sgdfstatus=0, sgdftime=0, sgdfformationid=0, sgdferror=''");
        $DB->delete_records('sgdf_log', ['action' => 'local_sgdf_completion:export']);
    }
    
    /**
     * rrenvoie le timestamp du dernier export enregistré dans les logs
     * @return int timestamp
     * @global \moodle_database $DB
     */
    private function get_last_export()
    {
        global $DB;

        $sql = "SELECT MAX(time) FROM {sgdf_log} WHERE action=?";
        $res = $DB->get_field_sql($sql, ['local_sgdf_completion:export'], IGNORE_MISSING);
        if (! $res) {
            return 0;
        }
        return (int) $res;
    }

    /**
     * enregistre l'export terminé dans les logs
     * 
     * @global \moodle_database $DB
     */
    private function log_export()
    {
        global $DB;
        $record = new \StdClass();
        $record->course = $this->courseid;
        $record->time = $this->processtime;
        $record->action = 'local_sgdf_completion:export';
        $DB->insert_record('sgdf_log', $record);
    }

    /**
     * helper function to display a character on a progressbar
     * @param int $minverb minimal verbosity to display character
     * @param string $text (can be a single char or even a short string)
     */
    private function vecho(int $minverb, string $text) {
        if ($this->verbose >= $minverb) echo $text; // progress bar
    }
}
